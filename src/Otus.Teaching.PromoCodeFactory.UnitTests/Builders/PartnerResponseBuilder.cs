﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerResponseBuilder
    {
        public static PartnerResponse CreateBase()
        {
            return new PartnerResponse()
            {
                Id = Guid.Parse("31a6a530-74b1-42f3-9727-066046f222bd"),
                Name = "PartnerName",
                IsActive = true,
                NumberIssuedPromoCodes = 5,
                PartnerLimits = new List<PartnerPromoCodeLimitResponse>()
            };
        }

        public static PartnerResponse WithId(this PartnerResponse partnerResponse, Guid id)
        {
            partnerResponse.Id = id;
            return partnerResponse;
        }

        public static PartnerResponse WithName(this PartnerResponse partnerResponse, string name)
        {
            partnerResponse.Name = name;
            return partnerResponse;
        }

        public static PartnerResponse IsInactive(this PartnerResponse partnerResponse)
        {
            partnerResponse.IsActive = false;
            return partnerResponse;
        }

        public static PartnerResponse WithIssuedPromoCodes(this PartnerResponse partnerResponse, int number)
        {
            partnerResponse.NumberIssuedPromoCodes = number;
            return partnerResponse;
        }

        public static PartnerResponse WithPartnerLimit(this PartnerResponse partnerResponse,
            PartnerPromoCodeLimitResponse limitResponse)
        {
            partnerResponse.PartnerLimits.Add(limitResponse);
            return partnerResponse;
        }
    }
}