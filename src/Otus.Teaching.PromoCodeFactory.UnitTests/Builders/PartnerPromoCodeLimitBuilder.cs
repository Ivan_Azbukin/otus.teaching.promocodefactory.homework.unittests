﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static PartnerPromoCodeLimit CreateBase()
        {
            return new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("7816c906-5d0e-4b57-ae5e-8defd45bb194"),
                Limit = 20,
                CreateDate = new DateTime(2021, 01, 01),
                EndDate = new DateTime(2021, 02, 01)
            };
        }

        public static PartnerPromoCodeLimit WithCancelDate(this PartnerPromoCodeLimit limit, DateTime cancelDate)
        {
            limit.CancelDate = cancelDate;
            return limit;
        }
    }
}