﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public  static class PartnerBuilder
    {
        public static Partner CreateBase()
        {
            return new Partner()
            {
                Id = Guid.Parse("e883b0d4-691c-4d18-b7ab-55a4c963ec4b"),
                Name = "PartnerName",
                IsActive = true,
                NumberIssuedPromoCodes = 5,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }

        public static Partner IsInactive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }

        public static Partner WithLimit(this Partner partner, PartnerPromoCodeLimit limit)
        {
            limit.PartnerId = partner.Id;
            limit.Partner = partner;
            partner.PartnerLimits.Add(limit);
            return partner;
        }

        public static Partner WithIssuedPromoCodes(this Partner partner, int number)
        {
            partner.NumberIssuedPromoCodes = number;
            return partner;
        }
    }
}