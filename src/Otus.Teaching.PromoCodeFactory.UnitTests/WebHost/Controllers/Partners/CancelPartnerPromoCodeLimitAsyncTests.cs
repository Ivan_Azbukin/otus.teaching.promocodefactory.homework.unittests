﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class CancelPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnerService> _partnerServiceMock;
        private readonly PartnersController _partnersController;

        public CancelPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerServiceMock = fixture.Freeze<Mock<IPartnerService>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = PartnerBuilder.CreateBase().Id;

            _partnerServiceMock.Setup(service => service.CancelPartnerPromoCodeLimitAsync(partnerId))
                .Throws<EntityNotFoundException>();
            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);
 
            // Assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }
        
        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
             var partnerId = PartnerBuilder.CreateBase().Id;

            _partnerServiceMock.Setup(service => service.CancelPartnerPromoCodeLimitAsync(partnerId))
                .Throws<SetPartnerPromoCodeLimitException>();

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}