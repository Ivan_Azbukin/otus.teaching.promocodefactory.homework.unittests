﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Dto
{
    public class SetPartnerPromoCodeLimitResponse
    {
        public Guid PartnerId { get; set; }
        
        public Guid LimitId { get; set; }
    }
}