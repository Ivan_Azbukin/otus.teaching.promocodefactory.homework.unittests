﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Dto
{
    public class SetPartnerPromoCodeLimitRequest
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}